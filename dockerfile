FROM ubuntu

LABEL MAINTAINER="Fred Oliveira"

LABEL APP-VERSON="1.0"

ENV NPM_VERSION=2 ENVIROMENT=PROD

RUN apt-get update && apt-get install -y git nano mpm

WORKDIR /usr/share/file

RUN mpe build

COPY requerimento.txt requerimento.txt

ADD .files.tar.gz ./

RUN useradd rarissa

USER rarissa

EXPOSE 8080

ENTRYPOINT [ "ping" ]

CMD [ "localhost" ]

VOLUME [ "/data" ]